<%-- 
    Document   : header
    Created on : 20 mars 2015, 10:27:32
    Author     : Elmahdi
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<header>
    <img src="./resources/img/user.png" />
    <span id="mainTitle">Gestionnaire d'utilisateurs</span>
    <div id="posLogin">
        <center>
            <c:if test="${!connecte}">
                <form action="ServletJeux" method="get" id=""formConnexion>
                    <table>
                        <tr>
                            <td>Connexion :</td>
                            <td><input type="text" name="log" placeholder="Login..." /></td>
                            <td><input type="password" name="pass" placeholder="Password..." /></td>
                            <td>
                                <input type="hidden" name="action" value="checkConnexion" />
                                <button type="submit" name="submit">Connexion</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <fieldset>
   <legend>Response from jQuery Ajax Request on Blur event</legend>
   <div id="ajaxResponse"></div>
 </fieldset>
            </c:if>
            <c:if test="${connecte}">
                    <table>
                        <tr>
                            <td><a href="ServletUsers?action=deconnexion">Deconnexion</a></td>
                        </tr>
                    </table>
            </c:if>
        </center>
    </div>
</header>