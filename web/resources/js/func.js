/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(initPage)
function initPage(){
}
function viewForm(_id){
    $("#darkFilter").fadeIn(200).promise().done(function( ) {
        $("#"+_id).fadeIn();
        $("#"+_id).animate({
            top:200+"px"
        })
        $("#"+_id).addClass("viewON");
    })
}

function doModif(_id,_login,_firstname,_lastname){
    viewForm(_id);
    $("#"+_id).find("input[name='login']").val(_login);
    $("#"+_id).find("input[name='nom']").val(_firstname);
    $("#"+_id).find("input[name='prenom']").val(_lastname);
}
function doSuppr(_id,_login){
    viewForm(_id);
    $("#"+_id).find("input[name='login']").val(_login);
}

function hideButton(_id){
    $("#"+_id).fadeOut();
}
function hideFilter(){
    $("fieldset.viewON").animate({
        top:-500+"px"
    },500,function(){
       $(this).hide().promise().done(function( ) {
            $("fieldset.viewON").removeClass("viewON");
            $("#darkFilter").fadeOut(200);
        })
    })
}