<%-- 
    Document   : ListesDesJeux
    Created on : 29 avr. 2015, 16:13:57
    Author     : hasso
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Jeux Video Html5</title>
    </head>
        <body>
        <header>
            <!--Logo & slogan-->
        </header>


         <section>
            <!--Contenu-->
            <a href="ServletJeux?action=creerDonneeDeTest"><h1>creer  des données test</h1></a>

            <table class="liste">
                <tr>
                    <th>Nom</th>
                    <th>Resume</th>
                   
                </tr>
                
                <c:forEach  items="${listeJeux}" var="j">
                    <tr>
                        
                        <td><b><a href="ServletJeux?action=AfficherDetails">${j.nom}</a></b></td>
                        <td>${j.description}</td>
                        
                    </tr>
                     
                </c:forEach>
                    
            </table>
        </section>
        </body>
    </html>
        