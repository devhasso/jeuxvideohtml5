<%-- 
    Document   : index
    Created on : 29 avr. 2015, 12:52:08
    Author     : hasso
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Jeux Video Html5</title>
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/style.css" />

        <script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/func.js"></script>
    </head>
    
    <body>
        <header>
            <!--Logo & slogan-->
        </header>

        <!--Menu-->
        <!--jsp:include page="header.jsp"/-->
        <jsp:include page="header.jsp"/>
        <jsp:include page="menu.jsp"/>
        <c:choose>
            <c:when  test="${param['action'] == 'AfficherTouslesJeux'}" >
                <jsp:include page="ListesDesJeux.jsp"/>
            </c:when>
            
            <c:when  test="${param['action'] == 'AfficherDetails'}" >
                <jsp:include page="afficherDetail.jsp"/>
            </c:when>
            
        </c:choose>
      
        <!--jsp:include page="footer.jsp"/-->
        
    </body>
</html>
