/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jeux.gestionnaires.GestionnairesJeux;
import jeux.gestionnaires.JeuxFacade;
import jeux.modeles.Jeux;

/**
 *
 * @author hasso
 */
@WebServlet(name = "ServletJeux", urlPatterns = {"/ServletJeux"})
public class ServletJeux extends HttpServlet {

    @EJB
    private JeuxFacade jeuxFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @EJB
    private GestionnairesJeux gestionnairesJeux;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession(true);
        String action = request.getParameter("action");
        String forwardTo = "";
        // Message à afficher
        String message = "";

        if (action != null) {
            //connexion à l'application
            if (action.equals("checkConnexion")) {
                if (request.getParameter("log").equals("app") && request.getParameter("pass").equals("app")) {
                    session.setAttribute("login", "app");
                    session.setAttribute("pass", "app");
                    session.setAttribute("connecte", true);
                    
                    forwardTo = "index.jsp?action=AfficherDetails";
                    message = "Connexion réussie !";
                } else {
                    session.setAttribute("connecte", false);
                    message = "Identifiant inexistants";
                }
            } else if (action.equals("deconnexion")) {
                session.setAttribute("connecte", false);
                message = "Déconnexion réussie, à bientôt !";
            }
           if(action.equals("creerDonneeDeTest")) {
               
                    jeuxFacade.creerDonneesDeTest();
                     Collection<Jeux> liste = jeuxFacade.findAll();
                   
                    
                    forwardTo = "index.jsp?action=AfficherTouslesJeux";
                     request.setAttribute("listeJeux", liste);
                    //response.sendRedirect("index.jsp?action=listerLesUtilisateurs");
                    message = "Créer des utilisateurs de test";
                   
           }
                
              if(action.equals("AfficherTouslesJeux")){
                  //jeuxFacade.creerDonneesDeTest();
                   
                    Collection<Jeux> liste = jeuxFacade.findAll();
                    request.setAttribute("listeJeux", liste);

                    forwardTo = "index.jsp?action=AfficherTouslesJeux";
                    //response.sendRedirect("index.jsp?action=listerLesUtilisateurs");
                    message = "Créer des utilisateurs de test";
                // créer un utilisateur : OK
                    
//                 RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message);
//                    dp.forward(request, response);
              } 
                
                if(action.equals("AfficherDetails")){
//                   
                    Jeux liste =gestionnairesJeux.chercher("say Viget");
                     //List<Jeux> liste = jeuxFacade.findAll();
                    // System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ #"+liste.get(0).getId());
                   

                    forwardTo = "index.jsp?action=AfficherDetails";
                     request.setAttribute("liste", liste);
                    //response.sendRedirect("index.jsp?action=listerLesUtilisateurs");
                    message = "Créer des utilisateurs de test";
                   
                    
            }
               
        }
 RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message);
                    dp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
