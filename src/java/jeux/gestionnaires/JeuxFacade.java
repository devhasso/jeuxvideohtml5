/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux.gestionnaires;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jeux.modeles.Auteur;
import jeux.modeles.Jeux;

/**
 *
 * @author hasso
 */
@Stateless
public class JeuxFacade extends AbstractFacade<Jeux> {
    @PersistenceContext(unitName = "JeuxvideoHtml5PU")
    private EntityManager em;
    
     @EJB
    private AuteurFacade auteurFacade;
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JeuxFacade() {
        super(Jeux.class);
    }
    
    public void creerDonneesDeTest() {
        System.out.println("CREER DONNEES DE TEST DEPUIS FACADE");
        /*for(int i =0; i < 1000; i++) {
            Film f = new Film("New York " + i);
            create(f);
        }
*/
              Jeux jeux =new Jeux("say Viget","Un mini-jeu de plate-forme pour apprendre à prononcer le nom de l’agence Viget", "/ressources/images/say.jpg", "http://sayviget.com/", "http://sayviget.com/", "netbeans");
               create(jeux);
               
        
        Auteur auteur = new Auteur();
        auteur.setNom("Steven Spielberg");
        auteurFacade.create(auteur);
        jeux.setAuteur(auteur);
                
        
         
        System.out.println("Nb films en base " + count());

        
    }
    
    
}
