/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeux.gestionnaires;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jeux.modeles.Auteur;
import jeux.modeles.Jeux;

/**
 *
 * @author hasso
 */
@Stateless
public class AuteurFacade extends AbstractFacade<Auteur> {
    @PersistenceContext(unitName = "JeuxvideoHtml5PU")
    private EntityManager em;

   
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuteurFacade() {
        super(Auteur.class);
    }
    
    
     
}
